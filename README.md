# BDD with SpecFlow: Writing Step Definitions

**Estimated reading time**: 15 min

## Story Outline
In this coding story, you will learn how to create the SpecFlow Step Definitions.

## Story Organization
**Story Branch**: main
> `git checkout main`

Tags: #Selenium, #.NET, #C_Sharp, #BDD, #SpecFlow
